<?php

/**
 * @file
 * Newsletter Assembler base file.
 */

/**
 * Implements hook_menu().
 */
function newsletter_assembler_menu() {
  $items = array();

  // Structure definitions.
  $items['admin/structure/newsletter-assembler-types'] = array(
    'title'            => 'Newsletter Assembler types',
    'description'      => 'Manage newsletter assembler types, including default status, front page promotion, comment settings, etc.',
    'page callback'    => 'newsletter_assembler_overview_types',
    'access arguments' => array('administer newsletter assembler types'),
    'file'             => 'newsletter_assembler.types.inc',
  );

  $items['admin/structure/newsletter-assembler-types/list'] = array(
    'title'  => 'List',
    'type'   => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );

  $items['admin/structure/newsletter-assembler-types/add'] = array(
    'title'            => 'Add newsletter assembler type',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('newsletter_assembler_type_form'),
    'access arguments' => array('administer newsletter assembler types'),
    'type'             => MENU_LOCAL_ACTION,
    'file'             => 'newsletter_assembler.types.inc',
  );

  $items['admin/structure/newsletter-assembler-types/manage/%newsletter_assembler_type'] = array(
    'title'            => 'Edit newsletter assembler type',
    'title callback'   => 'newsletter_assembler_type_page_title',
    'title arguments'  => array(4),
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('newsletter_assembler_type_form', 4),
    'access arguments' => array('administer newsletter assembler types'),
    'file'             => 'newsletter_assembler.types.inc',
  );

  $items['admin/structure/newsletter-assembler-types/manage/%newsletter_assembler_type/edit'] = array(
    'title'   => 'Edit',
    'type'    => MENU_DEFAULT_LOCAL_TASK,
    'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
  );

  $items['admin/structure/newsletter-assembler-types/manage/%newsletter_assembler_type/add-child'] = array(
    'title'            => 'Add newsletter assembler child type',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('newsletter_assembler_type_form', 4, 5),
    'access arguments' => array('administer newsletter assembler types'),
    'type'             => MENU_LOCAL_ACTION,
    'file'             => 'newsletter_assembler.types.inc',
  );

  $items['admin/structure/newsletter-assembler-types/manage/%newsletter_assembler_type/delete'] = array(
    'title'            => 'Delete',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('newsletter_assembler_type_delete_confirm', 4),
    'access arguments' => array('administer newsletter assembler types'),
    'type'             => MENU_LOCAL_TASK,
    'context'          => MENU_CONTEXT_INLINE,
    'weight'           => 10,
    'file'             => 'newsletter_assembler.types.inc',
  );

  if (newsletter_assembler_get_child_types()) {
    foreach (newsletter_assembler_get_child_types() as $key => $type) {

      $items['admin/structure/newsletter-assembler-types/manage-child/' . $type->type_base . ''] = array(
        'title'            => 'Newsletter Assembler Child types: !type',
        'title arguments'  => array('!type' => $type->type_base),
        'description'      => 'Manage newsletter assembler types, including default status, front page promotion, comment settings, etc.',
        'page callback'    => 'newsletter_assembler_child_overview_types',
        'access arguments' => array('administer newsletter assembler types'),
        'file'             => 'newsletter_assembler.types.inc',
      );

      $items['admin/structure/newsletter-assembler-types/manage-child/' . $type->type_base . '/list'] = array(
        'title'  => 'List',
        'type'   => MENU_DEFAULT_LOCAL_TASK,
        'weight' => -10,
      );

      $items['admin/structure/newsletter-assembler-types/manage-child/' . $type->type_base . '/manage/%newsletter_assembler_type'] = array(
        'title'            => 'Edit !type_base type: !name',
        'title arguments'  => array('!type_base' => $type->type_base, '!name' => $type->name),
        'page callback'    => 'drupal_get_form',
        'page arguments'   => array('newsletter_assembler_type_form', 6),
        'access arguments' => array('administer newsletter assembler types'),
        'file'             => 'newsletter_assembler.types.inc',
      );

      $items['admin/structure/newsletter-assembler-types/manage-child/' . $type->type_base . '/manage/%newsletter_assembler_type/edit'] = array(
        'title'   => 'Edit',
        'type'    => MENU_DEFAULT_LOCAL_TASK,
        'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
      );

      $items['admin/structure/newsletter-assembler-types/manage-child/' . $type->type_base . '/manage/%newsletter_assembler_type/delete'] = array(
        'title'            => 'Delete',
        'page callback'    => 'drupal_get_form',
        'page arguments'   => array('newsletter_assembler_type_delete_confirm_override', 6),
        'access arguments' => array('administer newsletter assembler types'),
        'type'             => MENU_LOCAL_TASK,
        'context'          => MENU_CONTEXT_INLINE,
        'weight'           => 10,
        'file'             => 'newsletter_assembler.types.inc',
      );
    }
  }

  $items['newsletter-assembler'] = array(
    'page callback'    => 'newsletter_assembler_page_default',
    'access arguments' => array('access content'),
    'menu_name'        => 'navigation',
    'type'             => MENU_CALLBACK,
  );

  $items['newsletter-assembler/add'] = array(
    'title'            => 'Add newsletter',
    'page callback'    => 'newsletter_assembler_add_page',
    'access arguments' => array('access content'),
    'file'             => 'newsletter_assembler.pages.inc',
  );

  foreach (newsletter_assembler_get_types() as $key => $type) {
    $items['newsletter-assembler/add/' . str_replace('_', '-', $type->type)] = array(
      'title'            => 'Create !name',
      'title arguments'  => array('!name' => $type->name),
      'page callback'    => 'drupal_get_form',
      'page arguments'   => array('newsletter_assembler_add', $type->type),
      'access callback'  => 'node_access',
      'access arguments' => array('create', $type->type),
      'description'      => $type->description,
      'file'             => 'newsletter_assembler.pages.inc',
    );
  }

  foreach (newsletter_assembler_get_child_types() as $key => $type) {
    $items['newsletter-assembler/add/' . str_replace('_', '-', $type->type)] = array(
      'title'            => 'Create !name',
      'title arguments'  => array('!name' => $type->name),
      'page callback'    => 'drupal_get_form',
      'page arguments'   => array('newsletter_assembler_add', $type->type),
      'access callback'  => 'node_access',
      'access arguments' => array('create', $type->type),
      'description'      => $type->description,
      'type'             => MENU_CALLBACK,
      'file'             => 'newsletter_assembler.pages.inc',
    );
  }

  $items['newsletter-assembler/%newsletter_assembler/edit'] = array(
    'title'            => 'Edit',
    'page callback'    => 'newsletter_assembler_edit',
    'page arguments'   => array(1),
    'access arguments' => array('update', 1),
    'weight'           => 0,
    'type'             => MENU_LOCAL_TASK,
    'context'          => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
    'file'             => 'newsletter_assembler.pages.inc',
  );

  return $items;
}

/**
 * Implements hook_menu_local_tasks_alter().
 */
function newsletter_assembler_menu_local_tasks_alter(&$data, $router_item, $root_path) {
  // Add action link to 'node/add' on 'admin/content' page.
  if ($root_path == 'admin/content') {
    $item = menu_get_item('newsletter-assembler/add');
    if ($item['access']) {
      $data['actions']['output'][] = array(
        '#theme' => 'menu_local_action',
        '#link'  => $item,
      );
    }
  }
}

/**
 * Title callback: Returns the unsanitized title.
 *
 * Of the newsletter type edit form.
 *
 * @param obj $type
 *   The newsletter type object.
 *
 * @return string
 *   An unsanitized string that is the title of the node type edit form.
 *
 * @see node_menu()
 */
function newsletter_assembler_type_page_title($type) {
  return $type->name;
}

/**
 * Implements hook_admin_paths().
 */
function newsletter_assembler_admin_paths() {
  if (variable_get('node_admin_theme')) {
    $paths = array(
      'newsletter-assembler/*/edit'               => TRUE,
      'newsletter-assembler/*/delete'             => TRUE,
      'newsletter-assembler/*/revisions'          => TRUE,
      'newsletter-assembler/*/revisions/*/revert' => TRUE,
      'newsletter-assembler/*/revisions/*/delete' => TRUE,
      'newsletter-assembler/add'                  => TRUE,
      'newsletter-assembler/add/*'                => TRUE,
    );
    return $paths;
  }
}

/**
 * Implements hook_permission().
 */
function newsletter_assembler_permission() {
  return array(
    'administer newsletter assembler types' => array(
      'title'           => t('Administer newsletter types'),
      'restrict access' => TRUE,
    ),
    'administer newsletters assembler' => array(
      'title'           => t('Administer newsletter'),
      'restrict access' => TRUE,
    ),
    'access newsletter assembler overview' => array(
      'title'       => t('Access the newsletter overview page'),
      'description' => t('Admin zone'),
    ),
    'access newsletter assembler' => array(
      'title' => t('View published newsletters'),
    ),
  );
}

/**
 * Implements hook_entity_info().
 */
function newsletter_assembler_entity_info() {
  $entity = array();

  $entity['newsletter_assembler_base'] = array(
    'label'            => t('Newsletter Assembler'),
    'controller class' => 'NewsletterAssemblerBaseEntityController',
    'base table'       => 'newsletter_assembler_base',
    'revision table'   => 'newsletter_assembler_base_revision',
    'uri callback'     => 'newsletter_assembler_uri',
    'load hook'        => 'newsletter_assembler_load',
    'fieldable'        => TRUE,
    'translation'      => array(
      'locale' => TRUE,
    ),
    'entity keys' => array(
      'id'       => 'newsletter_b_id',
      'bundle'   => 'type',
      'label'    => 'title',
      'revision' => 'newsletter_b_vid',
      'language' => 'language',
    ),
    'bundle keys' => array(
      'bundle' => 'type',
    ),
    'bundles' => array(),
    'view modes' => array(
      'full' => array(
        'label'           => t('Full display'),
        'custom settings' => FALSE,
      ),
      'teaser' => array(
        'label'           => t('Teaser'),
        'custom settings' => TRUE,
      ),
    ),
    'access callback' => 'newsletter_assembler_entity_access',
    'access arguments' => array(
      'user key' => 'uid',
      'access tag' => 'newsletter_assembler_access',
    ),
    'permission labels' => array(
      'singular' => t('Newsletter'),
      'plural'   => t('Newsletters'),
    ),
    // Add title replacement support for translations.
    'field replacement' => array(
      'title' => array(
        'field' => array(
          'type'         => 'text',
          'cardinality'  => 1,
          'translatable' => TRUE,
        ),
        'instance' => array(
          'label'    => t('Title'),
          'required' => TRUE,
          'settings' => array(
            'text_processing' => 0,
          ),
          'widget'   => array(
            'weight' => -5,
          ),
        ),
      ),
    ),
    // Support for inline entity form.
    'inline entity form' => array(
      'controller' => 'NewsletterAssemblerBaseInlineEntityFormController',
    ),
  );

  $entity['newsletter_assembler_child'] = array(
    'label'            => t('Newsletter Assembler Child'),
    'controller class' => 'NewsletterAssemblerChildEntityController',
    'base table'       => 'newsletter_assembler_child',
    'revision table'   => 'newsletter_assembler_child_revision',
    'uri callback'     => 'newsletter_assembler_uri',
    'load hook'        => 'newsletter_assembler_load',
    'fieldable'        => TRUE,
    'translation'      => array(
      'locale' => TRUE,
    ),
    'entity keys' => array(
      'id'       => 'newsletter_c_id',
      'bundle'   => 'type',
      'label'    => 'title',
      'revision' => 'newsletter_c_vid',
      'language' => 'language',
    ),
    'bundle keys' => array(
      'bundle' => 'type',
    ),
    'bundles' => array(),
    'view modes' => array(
      'full' => array(
        'label'           => t('Full display'),
        'custom settings' => FALSE,
      ),
      'teaser' => array(
        'label'           => t('Teaser'),
        'custom settings' => TRUE,
      ),
    ),
    'access callback' => 'newsletter_assembler_entity_access',
    'access arguments' => array(
      'user key' => 'uid',
      'access tag' => 'newsletter_assembler_access',
    ),
    'permission labels' => array(
      'singular' => t('Newsletter'),
      'plural'   => t('Newsletters'),
    ),
    // Add title replacement support for translations.
    'field replacement' => array(
      'title' => array(
        'field' => array(
          'type'         => 'text',
          'cardinality'  => 1,
          'translatable' => TRUE,
        ),
        'instance' => array(
          'label'    => t('Title'),
          'required' => TRUE,
          'settings' => array(
            'text_processing' => 0,
          ),
          'widget'   => array(
            'weight' => -5,
          ),
        ),
      ),
    ),
    // Support for inline entity form.
    'inline entity form' => array(
      'controller' => 'NewsletterAssemblerChildInlineEntityFormController',
    ),
  );

  // Token module support.
  if (module_exists('tokens')) {
    $entity['newsletter_assembler_base']['token type'] = 'newsletter-assembler-base';
    $entity['newsletter_assembler_child']['token type'] = 'newsletter-assembler-child';
  }

  // Bundles must provide a human readable name so we can create help and error
  // messages, and the path to attach Field admin pages to.
  if (newsletter_assembler_get_types()) {
    foreach (newsletter_assembler_get_types() as $key => $type) {
      $entity['newsletter_assembler_base']['bundles'][$type->type] = array(
        'label' => $type->name,
        'admin' => array(
          'path'             => 'admin/structure/newsletter-assembler-types/manage/%newsletter_assembler_type',
          'real path'        => 'admin/structure/newsletter-assembler-types/manage/' . $type->type,
          'bundle argument'  => 4,
          'access arguments' => array('administer newsletter assembler types'),
        ),
      );
    }
  }

  // Bundles must provide a human readable name so we can create help and error
  // messages, and the path to attach Field admin pages to.
  if (newsletter_assembler_get_child_types()) {
    foreach (newsletter_assembler_get_child_types() as $key => $type) {
      $entity['newsletter_assembler_child']['bundles'][$type->type] = array(
        'label' => $type->name,
        'admin' => array(
          'path'             => 'admin/structure/newsletter-assembler-types/manage-child/' . $type->type_base . '/manage/%newsletter_assembler_type',
          'real path'        => 'admin/structure/newsletter-assembler-types/manage-child/' . $type->type_base . '/manage/' . $type->type,
          'bundle argument'  => 6,
          'access arguments' => array('administer newsletter assembler types'),
        ),
      );
    }
  }

  return $entity;
}

/**
 * Entity uri callback: gives modules a chance to specify a path for a product.
 */
function newsletter_assembler_uri($newsletter) {
  return array(
    'path' => 'newsletter-assembler/' . $newsletter->newsletter_b_id,
  );
}

/**
 * TODO: Set documentation.
 */
function newsletter_assembler_object_prepare($newsletter_assembler) {

  if (!isset($newsletter_assembler->newsletter_b_id) || isset($newsletter_assembler->is_new)) {
    $newsletter_assembler->created = REQUEST_TIME;
  }
  else {
    // Experimental fase.
  }
}

/**
 * Load a newsletter assembler type.
 */
function newsletter_assembler_type_load($type_url_str) {
  $type_url_str = str_replace('-', '_', $type_url_str);

  $type = db_select('newsletter_assembler_base_type', 'nab_t')
    ->fields('nab_t')
    ->condition('nab_t.type', $type_url_str)
    ->execute()
    ->fetchAll();

  if (empty($type[0])) {
    $type = db_select('newsletter_assembler_child_type', 'nac_t')
      ->fields('nac_t')
      ->condition('nac_t.type', $type_url_str)
      ->execute()
      ->fetchAll();
  }

  return !empty($type[0]) ? $type[0] : FALSE;
}

/**
 * Load a newsletter assembler object.
 */
function newsletter_assembler_load($newsletter_b_id = NULL, $newsletter_b_vid = NULL) {

  $newsletter_b_id = (isset($newsletter_b_id) ? array($newsletter_b_id) : array());
  $conditions = (isset($newsletter_b_vid) ? array('newsletter_b_vid' => $newsletter_b_vid) : array());

  $newsletter_a = entity_load('newsletter_assembler_base', $newsletter_b_id, $conditions);

  return !empty($newsletter_a) ? $newsletter_a : FALSE;
}

/**
 * TODO: Rewrite the access method.
 */
function newsletter_assembler_entity_access() {
  return TRUE;
}

/**
 * TODO: newsletter_assembler_entity_access need work.
 */
function newsletter_assembler_access() {
  return newsletter_assembler_entity_access();
}

/**
 * TODO: Set documentation.
 */
function newsletter_assembler_type_set_defaults($info = array()) {
  $info = (array) $info;
  $new_type = $info + array(
    'type'        => '',
    'name'        => '',
    'description' => '',
    'help'        => '',
    'revision'    => 1,
  );
  $new_type = (object) $new_type;

  return $new_type;
}

/**
 * Internal function to get newsletter assembler types.
 *
 * @return object
 *   Return an object with the newsletter_assembler_type table.
 */
function newsletter_assembler_get_types() {
  $types = &drupal_static(__FUNCTION__, array());

  // Static cache.
  if (!empty($types)) {
    return $types;
  }

  $types = (object) NULL;

  $query = db_select('newsletter_assembler_base_type', 'nab_t')
    ->fields('nab_t')
    ->orderBy('nab_t.type', 'ASC');

  $types = $query->execute()->fetchAll();

  return $types;
}

/**
 * Internal function to get newsletter assembler types.
 *
 * @return object
 *   Return an object with the newsletter_assembler_type table.
 */
function newsletter_assembler_get_child_types() {
  $types = &drupal_static(__FUNCTION__, array());

  // Static cache.
  if (!empty($types)) {
    return $types;
  }

  $types = (object) NULL;

  $query = db_select('newsletter_assembler_child_type', 'nac_t')
    ->fields('nac_t')
    ->orderBy('nac_t.type', 'ASC');

  $types = $query->execute()->fetchAll();

  return $types;
}

/**
 * Saves a newsletter assembler type to the database.
 *
 * @param object $info
 *   The newsletter assembler type to save; an object with the following
 *   properties:
 *   - type: A string giving the machine name of the newsletter assembler type.
 *   - name: A string giving the human-readable name of the newsletter assembler
 *     type.
 *   - description: A string that describes the newsletter assembler type.
 *   - help: A string giving the help information shown to the user when
 *     creating a newsletter assembler of this type.
 *   - modified: TRUE or FALSE indicating whether this type has been modified by
 *     an administrator. Currently not used in any way.
 *   - locked: TRUE or FALSE indicating whether the administrator can change the
 *     machine name of this type.
 *   - disabled: TRUE or FALSE indicating whether this type has been disabled.
 *   - has_title: TRUE or FALSE indicating whether this type uses the newsletter
 *     assembler title field.
 *   - title_label: A string containing the label for the title.
 *   - module: A string giving the module defining this type of newsletter
 *     assembler.
 *   - orig_type: A string giving the original machine-readable name of this
 *     newsletter assembler type. This may be different from the current type
 *     name if the
 *     'locked' key is FALSE.
 *
 * @return int
 *   A status flag indicating the outcome of the operation, either SAVED_NEW or
 *   SAVED_UPDATED.
 */
function newsletter_assembler_save_type($info, $child = FALSE) {

  // Define which newsletter type to use.
  $newsletter_assembler_type = 'newsletter_assembler_base_type';

  // Define a base for child entity.
  $extra_child = array();

  if ($child) {
    $newsletter_assembler_type = 'newsletter_assembler_child_type';
    $extra_child = array('type_base' => (string) $info->type_base);
  }

  $is_existing = db_select($newsletter_assembler_type, 'na_t')
    ->fields('na_t', array('type'))
    ->condition('na_t.type', $info->type)
    ->execute()
    ->fetchField();

  $type = newsletter_assembler_type_set_defaults($info);

  $fields = $extra_child + array(
    'type'        => (string) $type->type,
    'name'        => (string) $type->name,
    'description' => (string) $type->description,
    'help'        => (string) $type->help,
    'revision'    => (int) $type->revision,
  );

  if ($is_existing) {
    db_update($newsletter_assembler_type)
      ->fields($fields)
      ->condition('type', $info->type)
      ->execute();

    if (!empty($type->old_type) && $type->old_type != $type->type) {
      field_attach_rename_bundle($newsletter_assembler_type, $type->old_type, $type->type);
    }

    // TODO: Create a hook system.
    // module_invoke_all('newsletter_assembler_type_update', $type);
    $status = SAVED_UPDATED;
  }
  else {
    db_insert($newsletter_assembler_type)
      ->fields($fields)
      ->execute();

    field_attach_create_bundle($newsletter_assembler_type, $type->type);

    // TODO: Create a hook system.
    // module_invoke_all('node_type_insert', $type);
    $status = SAVED_NEW;
  }

  return $status;
}
