<?php

/**
 * @file
 * Newsletter Assembler installer
 */

/**
 * Implements hook_schema().
 */
function newsletter_assembler_schema() {
  $schema = array();

  $schema['newsletter_assembler_base'] = array(
    'description' => 'Base table for assembled newsletters.',
    'fields' => array(
      'newsletter_b_id' => array(
        'description' => 'The primary identifier for a newsletter.',
        'type'        => 'serial',
        'unsigned'    => TRUE,
        'not null'    => TRUE,
      ),
      'newsletter_b_vid' => array(
        'description' => 'The current {newsletter_assembler_base_revision}.newsletter_b_id version identifier.',
        'type'        => 'int',
        'unsigned'    => TRUE,
        'not null'    => FALSE,
      ),
      'title' => array(
        'description' => 'The title of this assembled newsletters, always treated as non-markup plain text.',
        'type'        => 'varchar',
        'length'      => 255,
        'not null'    => TRUE,
        'default'     => '',
      ),
      'type' => array(
        'description' => 'The {newsletter_assembler_base_type}.type of this assembled newsletters.',
        'type'        => 'varchar',
        'length'      => 255,
        'not null'    => TRUE,
        'default'     => '',
      ),
      'language' => array(
        'description' => 'The {languages}.language of this assembled newsletters.',
        'type'        => 'varchar',
        'length'      => 32,
        'not null'    => TRUE,
        'default'     => '',
      ),
      'uid' => array(
        'description' => 'The {users}.uid that created this assembled newsletters.',
        'type'        => 'int',
        'not null'    => TRUE,
        'default'     => 0,
      ),
      'status' => array(
        'description' => 'Boolean indicating whether or not the assembled newsletters appears in lists and may be added to orders.',
        'type'        => 'int',
        'size'        => 'tiny',
        'not null'    => TRUE,
        'default'     => 1,
      ),
      'created' => array(
        'description' => 'The Unix timestamp when the assembled newsletters was created.',
        'type'        => 'int',
        'not null'    => TRUE,
        'default'     => 0,
      ),
      'changed' => array(
        'description' => 'The Unix timestamp when the assembled newsletters was most recently saved.',
        'type'        => 'int',
        'not null'    => TRUE,
        'default'     => 0,
      ),
      'data' => array(
        'type'        => 'blob',
        'not null'    => FALSE,
        'size'        => 'big',
        'serialize'   => TRUE,
        'description' => 'A serialized array of additional data.',
      ),
    ),
    'indexes' => array(
      'newsletter_assembler_base_type' => array('type'),
      'uid'                            => array('uid'),
      'created'                        => array('created'),
    ),
    'unique keys' => array(
      'newsletter_b_vid' => array('newsletter_b_vid'),
    ),
    'foreign keys' => array(
      'current_revision' => array(
        'table'   => 'newsletter_assembler_base_revision',
        'columns' => array('newsletter_b_vid' => 'newsletter_b_vid'),
      ),
      'creator' => array(
        'table'   => 'users',
        'columns' => array('uid' => 'uid'),
      ),
    ),
    'primary key' => array('newsletter_b_id'),
  );

  $schema['newsletter_assembler_base_revision'] = array(
    'description' => 'Saves information about each saved revision of a {newsletter_assembler_base}.',
    'fields' => array(
      'newsletter_b_id' => array(
        'description' => 'The {newsletter_assembler_base}.newsletter_b_id of the assembled newsletters this revision belongs to.',
        'type'        => 'int',
        'unsigned'    => TRUE,
        'not null'    => TRUE,
        'default'     => 0,
      ),
      'newsletter_b_vid' => array(
        'description' => 'The primary identifier for this revision.',
        'type'        => 'serial',
        'unsigned'    => TRUE,
        'not null'    => TRUE,
      ),
      'title' => array(
        'description' => 'The title of this assembled newsletters for this revision',
        'type'        => 'varchar',
        'length'      => 255,
        'not null'    => TRUE,
        'default'     => '',
      ),
      'revision_uid' => array(
        'description' => 'The {users}.uid that owns the assembled newsletters at this revision.',
        'type'        => 'int',
        'not null'    => TRUE,
        'default'     => 0,
      ),
      'status' => array(
        'description' => 'The status of this revision.',
        'type'        => 'int',
        'size'        => 'tiny',
        'not null'    => TRUE,
        'default'     => 1,
      ),
      'log' => array(
        'description' => 'The log entry explaining the changes in this version.',
        'type'        => 'text',
        'not null'    => TRUE,
        'size'        => 'big',
      ),
      'revision_timestamp' => array(
        'description' => 'The Unix timestamp when this revision was created.',
        'type'        => 'int',
        'not null'    => TRUE,
        'default'     => 0,
      ),
      'data' => array(
        'type'        => 'blob',
        'not null'    => FALSE,
        'size'        => 'big',
        'serialize'   => TRUE,
        'description' => 'A serialized array of additional data for this revision.',
      ),
    ),
    'primary key' => array('newsletter_b_vid'),
    'indexes' => array(
      'newsletter_b_id' => array('newsletter_b_id'),
      'revision_uid'  => array('revision_uid'),
    ),
    'foreign keys' => array(
      'newsletter' => array(
        'table'   => 'newslette_baser_assembler',
        'columns' => array('newsletter_b_id' => 'newsletter_b_id'),
      ),
      'owner' => array(
        'table'   => 'users',
        'columns' => array('revision_uid' => 'uid'),
      ),
    ),
  );

  $schema['newsletter_assembler_base_type'] = array(
    'description' => 'Stores information about all defined {newsletter_assembler_base} types.',
    'fields' => array(
      'type' => array(
        'description' => 'The machine-readable name of this type.',
        'type'        => 'varchar',
        'length'      => 42,
        'not null'    => TRUE,
        'default'     => '',
      ),
      'name' => array(
        'description' => 'The human-readable name of this type.',
        'type'        => 'varchar',
        'length'      => 255,
        'not null'    => TRUE,
        'default'     => '',
      ),
      'description' => array(
        'description' => 'A brief description of this type.',
        'type'        => 'text',
        'not null'    => TRUE,
        'size'        => 'medium',
      ),
      'help' => array(
        'description' => 'Help information shown to the user when creating a {newsletter_assembler_base} of this type.',
        'type'        => 'text',
        'not null'    => TRUE,
        'size'        => 'medium',
      ),
      'revision' => array(
        'description' => 'Determine whether to create a new revision when an assembled newsletters of this type is updated.',
        'type'        => 'int',
        'size'        => 'tiny',
        'not null'    => TRUE,
        'default'     => 1,
      ),
    ),
    'primary key' => array('type'),
  );

  $schema['newsletter_assembler_child'] = array(
    'description' => 'Child table for assembled newsletters base.',
    'fields' => array(
      'newsletter_c_id' => array(
        'description' => 'The primary identifier for a newsletter.',
        'type'        => 'serial',
        'unsigned'    => TRUE,
        'not null'    => TRUE,
      ),
      'newsletter_c_vid' => array(
        'description' => 'The current {newsletter_assembler_child_revision}.newsletter_c_id version identifier.',
        'type'        => 'int',
        'unsigned'    => TRUE,
        'not null'    => FALSE,
      ),
      'title' => array(
        'description' => 'The title of this assembled newsletters, always treated as non-markup plain text.',
        'type'        => 'varchar',
        'length'      => 255,
        'not null'    => TRUE,
        'default'     => '',
      ),
      'type' => array(
        'description' => 'The {newsletter_assembler_child_type}.type of this assembled newsletters.',
        'type'        => 'varchar',
        'length'      => 255,
        'not null'    => TRUE,
        'default'     => '',
      ),
      'language' => array(
        'description' => 'The {languages}.language of this assembled newsletters.',
        'type'        => 'varchar',
        'length'      => 32,
        'not null'    => TRUE,
        'default'     => '',
      ),
      'uid' => array(
        'description' => 'The {users}.uid that created this assembled newsletters.',
        'type'        => 'int',
        'not null'    => TRUE,
        'default'     => 0,
      ),
      'status' => array(
        'description' => 'Boolean indicating whether or not the assembled newsletters appears in lists and may be added to orders.',
        'type'        => 'int',
        'size'        => 'tiny',
        'not null'    => TRUE,
        'default'     => 1,
      ),
      'created' => array(
        'description' => 'The Unix timestamp when the assembled newsletters was created.',
        'type'        => 'int',
        'not null'    => TRUE,
        'default'     => 0,
      ),
      'changed' => array(
        'description' => 'The Unix timestamp when the assembled newsletters was most recently saved.',
        'type'        => 'int',
        'not null'    => TRUE,
        'default'     => 0,
      ),
      'data' => array(
        'type'        => 'blob',
        'not null'    => FALSE,
        'size'        => 'big',
        'serialize'   => TRUE,
        'description' => 'A serialized array of additional data.',
      ),
    ),
    'indexes' => array(
      'newsletter_assembler_child_type' => array('type'),
      'uid'                             => array('uid'),
      'created'                         => array('created'),
    ),
    'unique keys' => array(
      'newsletter_c_vid' => array('newsletter_C_vid'),
    ),
    'foreign keys' => array(
      'current_revision' => array(
        'table'   => 'newsletter_assembler_child_revision',
        'columns' => array('newsletter_c_vid' => 'newsletter_c_vid'),
      ),
      'creator' => array(
        'table'   => 'users',
        'columns' => array('uid' => 'uid'),
      ),
    ),
    'primary key' => array('newsletter_c_id'),
  );

  $schema['newsletter_assembler_child_revision'] = array(
    'description' => 'Saves information about each saved revision of a {newsletter_assembler_child}.',
    'fields' => array(
      'newsletter_c_id' => array(
        'description' => 'The {newsletter_assembler_child}.newsletter_c_id of the assembled newsletters this revision belongs to.',
        'type'        => 'int',
        'unsigned'    => TRUE,
        'not null'    => TRUE,
        'default'     => 0,
      ),
      'newsletter_c_vid' => array(
        'description' => 'The primary identifier for this revision.',
        'type'        => 'serial',
        'unsigned'    => TRUE,
        'not null'    => TRUE,
      ),
      'title' => array(
        'description' => 'The title of this assembled newsletters for this revision',
        'type'        => 'varchar',
        'length'      => 255,
        'not null'    => TRUE,
        'default'     => '',
      ),
      'revision_uid' => array(
        'description' => 'The {users}.uid that owns the assembled newsletters at this revision.',
        'type'        => 'int',
        'not null'    => TRUE,
        'default'     => 0,
      ),
      'status' => array(
        'description' => 'The status of this revision.',
        'type'        => 'int',
        'size'        => 'tiny',
        'not null'    => TRUE,
        'default'     => 1,
      ),
      'log' => array(
        'description' => 'The log entry explaining the changes in this version.',
        'type'        => 'text',
        'not null'    => TRUE,
        'size'        => 'big',
      ),
      'revision_timestamp' => array(
        'description' => 'The Unix timestamp when this revision was created.',
        'type'        => 'int',
        'not null'    => TRUE,
        'default'     => 0,
      ),
      'data' => array(
        'type'        => 'blob',
        'not null'    => FALSE,
        'size'        => 'big',
        'serialize'   => TRUE,
        'description' => 'A serialized array of additional data for this revision.',
      ),
    ),
    'primary key' => array('newsletter_c_vid'),
    'indexes' => array(
      'newsletter_c_id' => array('newsletter_c_id'),
      'revision_uid'  => array('revision_uid'),
    ),
    'foreign keys' => array(
      'newsletter' => array(
        'table'   => 'newsletter_assembler_child',
        'columns' => array('newsletter_c_id' => 'newsletter_c_id'),
      ),
      'owner' => array(
        'table'   => 'users',
        'columns' => array('revision_uid' => 'uid'),
      ),
    ),
  );

  $schema['newsletter_assembler_child_type'] = array(
    'description' => 'Stores information about all defined {newsletter_assembler_child} types.',
    'fields' => array(
      'type' => array(
        'description' => 'The machine-readable name of this type.',
        'type'        => 'varchar',
        'length'      => 42,
        'not null'    => TRUE,
        'default'     => '',
      ),
      'type_base' => array(
        'description' => 'The machine-readable name of the base type.',
        'type'        => 'varchar',
        'length'      => 42,
        'not null'    => TRUE,
        'default'     => '',
      ),
      'name' => array(
        'description' => 'The human-readable name of this type.',
        'type'        => 'varchar',
        'length'      => 255,
        'not null'    => TRUE,
        'default'     => '',
      ),
      'description' => array(
        'description' => 'A brief description of this type.',
        'type'        => 'text',
        'not null'    => TRUE,
        'size'        => 'medium',
      ),
      'help' => array(
        'description' => 'Help information shown to the user when creating a {newsletter_assembler_child} of this type.',
        'type'        => 'text',
        'not null'    => TRUE,
        'size'        => 'medium',
      ),
      'revision' => array(
        'description' => 'Determine whether to create a new revision when an assembled newsletters of this type is updated.',
        'type'        => 'int',
        'size'        => 'tiny',
        'not null'    => TRUE,
        'default'     => 1,
      ),
    ),
    'primary key' => array('type'),
  );

  return $schema;
}
