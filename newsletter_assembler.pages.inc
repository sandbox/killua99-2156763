<?php

/**
 * @file
 * Newsletter Assembler.
 */

/**
 * TODO: Set documentation.
 */
function newsletter_assembler_add($form, &$form_state, $newsletter_assembler) {
  global $user;

  if (empty($langcode)) {
    $langcode = LANGUAGE_NONE;
  }

  $newsletter_assembler_type = newsletter_assembler_type_load($newsletter_assembler);

  $newsletter_assembler = (object) ((array) $newsletter_assembler_type + array(
    'uid'  => $user->uid,
    'name' => $user->name,
  ));

  newsletter_assembler_object_prepare($newsletter_assembler);

  field_attach_form('newsletter_assembler_base', $newsletter_assembler, $form, $form_state, $langcode);

  $form['actions'] = array(
    '#type'   => 'actions',
    '#weight' => 40,
  );

  $submit = array();
  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions']['submit'] = array(
    '#type'   => 'submit',
    '#value'  => t('Save !name', array('!name' => $newsletter_assembler->name)),
    '#submit' => array_merge($submit, array('newsletter_assembler_add_submit')),
  );

  $form['actions']['save_continue'] = array(
    '#type'   => 'submit',
    '#value'  => t('Save and add !name', array('!name' => $newsletter_assembler->name)),
    '#suffix' => l(t('Cancel'), 'newsletter-assembler-types/add'),
    '#submit' => array_merge($submit, array('newsletter_assembler_type_form_submit')),
    '#weight' => 45,
  );

  return $form;
}

/**
 * TODO: Set documentation.
 */
function newsletter_assembler_add_submit($form, $form_state) {

}

/**
 * TODO: Set documentation.
 */
function newsletter_assembler_edit() {

}

/**
 * TODO: Set documentation.
 */
function newsletter_assembler_add_page() {
  $item = menu_get_item();
  $content = system_admin_menu_block($item);

  return theme('node_add_list', array('content' => $content));
}
