<?php

/**
 * @file
 * Newsletter assembler type editing user interface.
 */

/**
 * Displays the newsletter assembler type admin overview page.
 */
function newsletter_assembler_overview_types() {

  $field_ui = module_exists('field_ui');
  $header = array(
    t('Name'),
    array(
      'data'    => t('Operations'),
      'colspan' => $field_ui ? '4' : '2',
    ),
  );
  $rows = array();

  $newsletter_assembler_types = newsletter_assembler_get_types();

  if (!empty($newsletter_assembler_types)) {
    foreach ($newsletter_assembler_types as $key => $type) {
      $type_url_str = $type->type;
      $row = array(theme('node_admin_overview', array(
        'name' => $type->name,
        'type' => $type,
      )));

      $links = menu_contextual_links('newsletter-assembler', 'admin/structure/newsletter-assembler-types/manage', array($type_url_str));

      $row[] = array(
        'data' => theme('links', array(
          'links'      => $links,
          'attributes' => array(
            'class' => 'links inline operations',
          ),
      )));

      $na_child = db_select('newsletter_assembler_child_type', 'nac_t')
        ->fields('nac_t', array('type'))
        ->condition('nac_t.type_base', $type->type)
        ->execute()
        ->fetchAll();

      if (!empty($na_child)) {
        $row[] = array('data' => l(t('manage childs type'), 'admin/structure/newsletter-assembler-types/manage-child/' . $type_url_str));
      }

      if ($field_ui) {
        // Manage fields.
        $row[] = array('data' => l(t('manage fields'), 'admin/structure/newsletter-assembler-types/manage/' . $type_url_str . '/fields'));

        // Display fields.
        $row[] = array('data' => l(t('manage display'), 'admin/structure/newsletter-assembler-types/manage/' . $type_url_str . '/display'));
      }

      $rows[] = $row;
    }
  }

  $build['newsletter_assembler_table'] = array(
    '#theme'  => 'table',
    '#header' => $header,
    '#rows'   => $rows,
    '#empty'  => t('No newsletter assembler types available. <a href="@link">Add newsletter assembler type</a>.', array('@link' => url('admin/structure/newsletter-assembler-types/add'))),
  );

  return $build;
}

/**
 * Displays the newsletter assembler child type admin overview page.
 */
function newsletter_assembler_child_overview_types() {

  $field_ui = module_exists('field_ui');
  $header = array(
    t('Name'),
    array(
      'data'    => t('Operations'),
      'colspan' => $field_ui ? '4' : '2',
    ),
  );
  $rows = array();

  $newsletter_assembler_types = newsletter_assembler_get_child_types();

  if (!empty($newsletter_assembler_types)) {
    foreach ($newsletter_assembler_types as $key => $type) {
      if (arg(4) !== $type->type_base) {
        continue;
      }
      $type_url_str = $type->type;
      $row = array(theme('node_admin_overview', array(
        'name' => $type->name,
        'type' => $type,
      )));

      $links = menu_contextual_links('newsletter-assembler', "admin/structure/newsletter-assembler-types/manage-child/{$type->type_base}/manage", array($type_url_str));

      $row[] = array(
        'data' => theme('links', array(
          'links'      => $links,
          'attributes' => array(
            'class' => 'links inline operations',
          ),
      )));

      if ($field_ui) {
        // Manage fields.
        $row[] = array('data' => l(t('manage fields'), "admin/structure/newsletter-assembler-types/manage-child/{$type->type_base}/manage/" . $type_url_str . '/fields'));

        // Display fields.
        $row[] = array('data' => l(t('manage display'), "admin/structure/newsletter-assembler-types/manage-child/{$type->type_base}/manage/" . $type_url_str . '/display'));
      }

      $rows[] = $row;
    }
  }

  $build['newsletter_assembler_table'] = array(
    '#theme'  => 'table',
    '#header' => $header,
    '#rows'   => $rows,
    '#empty'  => t('No newsletter assembler types available. <a href="@link">Add newsletter assembler type</a>.', array('@link' => url("admin/structure/newsletter-assembler-types/manage/{$type->type_base}/add-child"))),
  );

  return $build;
}

/**
 * Override funcion to order the form action.
 *
 * We need to to a ugly override to extend the child entity.
 */
function newsletter_assembler_type_form_override($form, &$form_state, $type) {

  return newsletter_assembler_type_form($form, $form_state, $type);
}

/**
 * TODO: Documentation.
 *
 * @return array
 *   Drupal form.
 */
function newsletter_assembler_type_form($form, &$form_state, $type = NULL, $action = '') {
  module_load_include('inc', 'newsletter_assembler', 'includes/newsletter_assembler');

  if (!isset($type->type)) {
    // This is a new type. Node module managed types are custom and unlocked.
    $type = newsletter_assembler_type_set_defaults(array(
      'custom' => 1,
      'locked' => 0,
    ));
  }
  $form['#newsletter_assembler_type'] = $type;

  if ($action === 'add-child') {
    // Keep the parent type.
    $form['type_base'] = array(
      '#type'  => 'hidden',
      '#value' => $type->type,
    );

    // Refresh the type.
    $type = newsletter_assembler_type_set_defaults(array(
      'custom' => 1,
      'locked' => 0,
    ));
  }

  if (!empty($type->type_base)) {
    // This mean we're on a child entity situation.
    $form['type_base'] = array(
      '#type'  => 'hidden',
      '#value' => $type->type_base,
    );
  }

  _newsletter_assembler_get_all_bundles();

  $form['name'] = array(
    '#title'         => t('Name'),
    '#description'   => t('The human-readable name of this newsletter assembler type. This text will be displayed as part of the list on the <em>Add new newsletter assembler</em> page. It is recommended that this name begin with a capital letter and contain only letters, numbers, and spaces. This name must be unique.'),
    '#type'          => 'textfield',
    '#default_value' => $type->name,
    '#required'      => TRUE,
    '#size'          => 30,
  );

  $form['type'] = array(
    '#description'   => t('A unique machine-readable name for this newsletter assembler type. It must only contain lowercase letters, numbers, and underscores. This name will be used for constructing the URL of the %newsletter-assembler-add page, in which underscores will be converted into hyphens.', array(
      '%newsletter-assembler-add' => t('Add new newsletter assembler'),
    )),
    '#type'          => 'machine_name',
    '#default_value' => $type->type,
    '#maxlength'     => 42,
    '#machine_name'  => array(
      'exists' => 'node_type_load',
    ),
  );

  $form['description'] = array(
    '#title'         => t('Description'),
    '#description'   => t('Describe this newsletter assembler type. The text will be displayed on the <em>Add new newsletter assembelr</em> page.'),
    '#type'          => 'textarea',
    '#default_value' => $type->description,
  );

  $form['help'] = array(
    '#title'         => t('Explanation or submission guidelines'),
    '#description'   => t('This text will be displayed at the top of the page when creating or editing newsletter assembler of this type.'),
    '#type'          => 'textarea',
    '#default_value' => $type->help,
    '#rows'          => 3,
  );

  $form['revision'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Default newsletter assembler of this type to be saved as new revisions when edited.'),
    '#default_value' => $type->revision,
  );

  $form['actions'] = array(
    '#type'   => 'actions',
    '#weight' => 40,
  );

  $submit = array();
  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions']['submit'] = array(
    '#type'   => 'submit',
    '#value'  => t('Save newsletter assembler type'),
    '#submit' => array_merge($submit, array('newsletter_assembler_type_form_submit')),
  );

  if (!empty($type->type)) {
    $form['actions']['delete'] = array(
      '#type'   => 'submit',
      '#value'  => t('Delete newsletter assembler type'),
      '#suffix' => l(t('Cancel'), 'admin/structure/newsletter-assembler-types'),
      '#submit' => array_merge($submit, array('newsletter_assembler_type_form_delete_submit')),
      '#weight' => 45,
    );
  }
  else {
    $form['actions']['save_continue'] = array(
      '#type'   => 'submit',
      '#value'  => t('Save and add fields'),
      '#suffix' => l(t('Cancel'), 'admin/structure/newsletter-assembler-types'),
      '#submit' => array_merge($submit, array('newsletter_assembler_type_form_submit')),
      '#weight' => 45,
    );
  }

  $form['#validate'][] = 'newsletter_assembler_type_form_validate';

  $form['old_type'] = array(
    '#type'  => 'value',
    '#value' => $type->type,
  );

  return $form;
}

/**
 * Form submission handler for newsletter_assembler_type_form().
 *
 * @see newsletter_assembler_type_form()
 * @see newsletter_assembler_type_form_submit()
 */
function newsletter_assembler_type_form_validate($form, &$form_state) {
  $type = (object) NULL;
  $type->type = trim($form_state['values']['type']);
  $type->name = trim($form_state['values']['name']);

  $newsletter_assembler_types = newsletter_assembler_get_types();
  // If saving a new newsletter assembler type, ensure it has a unique machine
  // name.
  if (!empty($newsletter_assembler_types->name) && !in_array($type->type, $newsletter_assembler_types)) {
    form_set_error('type', t('The machine name specified is already in use.'));
  }
}

/**
 * Form submission handler for newsletter_assembler_type_form().
 *
 * @see newsletter_assembler_type_form()
 * @see newsletter_assembler_type_form_validate()
 */
function newsletter_assembler_type_form_submit($form, &$form_state) {

  $child = FALSE;
  if ($form_state['triggering_element']['#parents'][0] == 'delete') {
    $form_state['redirect'] = 'admin/structure/newsletter-assembler-types/manage/' . str_replace('_', '-', trim($form_state['values']['type'])) . '/delete';
    return;
  }

  $type = $form_state['complete form']['#newsletter_assembler_type'];

  $type->name        = trim($form_state['values']['name']);
  $type->type        = trim($form_state['values']['type']);
  $type->description = trim($form_state['values']['description']);
  $type->help        = trim($form_state['values']['help']);
  $type->revision    = trim($form_state['values']['revision']);

  if (!empty($form_state['values']['type_base'])) {
    $child = TRUE;
    $type->type_base = trim($form_state['values']['type_base']);
  }
  // Saving process.
  $status = newsletter_assembler_save_type($type, $child);

  $t_args = array('%name' => $type->name);

  menu_rebuild();
  if ($status === SAVED_UPDATED) {
    drupal_set_message(t('The newsletter assembler type %name has been updated.', $t_args));
  }
  elseif ($status === SAVED_NEW) {
    node_add_body_field($type);
    drupal_set_message(t('The newsletter assembler type %name has been added.', $t_args));
    watchdog('newsletter_assembler', 'Added newsletter assembler type %name.', $t_args, WATCHDOG_NOTICE, l(t('view'), 'admin/structure/newsletter-assembler-types'));
  }

  if ($form_state['triggering_element']['#parents'][0] === 'save_continue') {
    $form_state['redirect'] = 'admin/structure/newsletter-assembler-types/manage/' . str_replace('_', '-', $type->type) . '/fields';
  }
  else {
    $form_state['redirect'] = 'admin/structure/newsletter-assembler-types';
  }
}

/**
 * Form submission handler for newsletter_assembler_type_form().
 *
 * @see newsletter_assembler_type_form_validate()
 */
function newsletter_assembler_type_form_delete_submit($form, &$form_state) {

}

/**
 * TODO: Documentation.
 */
function newsletter_assembler_type_delete_confirm_override($form, &$form_state, $type_base = NULL, $type = NULL) {
  $form['type'] = array(
    '#type'  => 'value',
    '#value' => $type->type,
  );
  $form['name'] = array(
    '#type'  => 'value',
    '#value' => $type->name,
  );

  $message = t('Are you sure you want to delete the newsletter assembler type %type?', array('%type' => $type->name));
  $caption = '';

  $num_nodes = db_select('newsletter_assembler_child', 'na_c')
    ->fields('na_c', array('newsletter_b_id'))
    ->condition('na_c.type', $type->type)
    ->countQuery()->execute()->fetchField();

  if ($num_nodes) {
    $caption .= '<p>' . format_plural($num_nodes, '%type is used by 1 piece of newsletter on your site. If you remove this newsletter assembler type, you will not be able to edit the %type content and it may not display correctly.', '%type is used by @count pieces of newsletter on your site. If you remove %type, you will not be able to edit the %type content and it may not display correctly.', array('%type' => $type->name)) . '</p>';
  }

  $caption .= '<p><strong>' . t('This action cannot be undone.') . '</strong></p>';

  return confirm_form($form, $message, 'admin/structure/newsletter-assembler-types', $caption, t('Delete'), t('Cancel'));
}

/**
 * Delete confirmation.
 *
 * @ingroup forms
 */
function newsletter_assembler_type_delete_confirm($form, &$form_state, $type = NULL) {
  $form['type'] = array(
    '#type'  => 'value',
    '#value' => $type->type,
  );
  $form['name'] = array(
    '#type'  => 'value',
    '#value' => $type->name,
  );

  $message = t('Are you sure you want to delete the newsletter assembler type %type?', array('%type' => $type->name));
  $caption = '';

  $num_nodes = db_select('newsletter_assembler_base', 'na_b')
    ->fields('na_b', array('newsletter_b_id'))
    ->condition('na_b.type', $type->type)
    ->countQuery()->execute()->fetchField();

  if ($num_nodes) {
    $caption .= '<p>' . format_plural($num_nodes, '%type is used by 1 piece of newsletter on your site. If you remove this newsletter assembler type, you will not be able to edit the %type content and it may not display correctly.', '%type is used by @count pieces of newsletter on your site. If you remove %type, you will not be able to edit the %type content and it may not display correctly.', array('%type' => $type->name)) . '</p>';
  }

  $caption .= '<p><strong>' . t('This action cannot be undone.') . '</strong></p>';

  return confirm_form($form, $message, 'admin/structure/newsletter-assembler-types', $caption, t('Delete'), t('Cancel'));
}

/**
 * Process newsletter assembler type delete confirm submissions.
 *
 * @see newsletter_assembler_type_delete_confirm()
 */
function newsletter_assembler_type_delete_confirm_submit($form, &$form_state) {

  // TODO: Need to create the delete newsletter assembler type.
  // newsletter_assembler_type_delete($form_state['values']['type']);

  drupal_set_message(t('The newsletter assembler type %name has been deleted.', $t_args));
  watchdog('newsletter_assembler', 'Deleted newsletter assembler type %name.', $t_args, WATCHDOG_NOTICE);

  // newsletter_assembler_types_rebuild();
  menu_rebuild();

  $form_state['redirect'] = 'admin/structure/types';
}
