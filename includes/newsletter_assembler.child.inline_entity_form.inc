<?php

/**
 * @file
 * Defines the inline entity form controller for Newsletter Assembler.
 */

class NewsletterAssemblerChildInlineEntityFormController extends EntityInlineEntityFormController {

  /**
   * Overrides EntityInlineEntityFormController::defaultLabels().
   */
  public function defaultLabels() {
    $labels = array(
      'singular' => t('Newsletter assembler Child'),
      'plural'   => t('Newsletters assembler Childs'),
    );

    return $labels;
  }

  /**
   * Overrides EntityInlineEntityFormController::settingsForm().
   */
  public function settingsForm($field, $instance) {
    $form = parent::settingsForm($field, $instance);

    if (!empty($field['settings']['handler_settings']['target_bundles'])) {
      foreach ($field['settings']['handler_settings']['target_bundles'] as $bundle) {
        $newsletter_assembler_child_type = newsletter_assembler_type_load($bundle);

        $instances = field_info_instances($field['settings']['target_type'], $bundle);

        if (!empty($instances)) {
          $form['select_labels'] = array(
            '#title'         => t('Select which field to be default labesl.'),
            '#type'          => 'checkbox',
            '#default_value' => !empty($this->settings['select_labels']) ? $this->settings['select_labels'] : 0,
          );

          foreach ($instances as $key => $instance) {
            $form['fields_labes'][$key] = array(
              '#title'         => $instance['label'],
              '#type'          => 'checkbox',
              '#default_value' => !empty($this->settings['fields_labes'][$key]) ? $this->settings['fields_labes'][$key] : 0,
              '#states'        => array(
                'visible' => array(
                  ':input[name="instance[widget][settings][type_settings][select_labels]"]' => array('checked' => TRUE),
                ),
              ),
            );
          }
        }
      }
    }

    return $form;
  }

  /**
   * Overrides EntityInlineEntityFormController::entityForm().
   */
  public function entityForm($entity_form, &$form_state) {
    global $user;

    $newsletter_a = $entity_form['#entity'];
    $newsletter_assembler_child_type = newsletter_assembler_type_load($newsletter_a->type);

    $newsletter_assembler_child_type = (object) ((array) $newsletter_assembler_child_type + array(
      'uid'  => $user->uid,
    ));

    $instances = field_info_instances('newsletter_assembler_child', $newsletter_a->type);

    if (!empty($this->settings['fields_labes'])) {
      foreach ($this->settings['fields_labes'] as $key => $value) {
        $instances[$key]['#required'] = $value;
      }
    }

    // $entity_form = $instances + $entity_form;

    dpm($entity_form);
    $entity_form['title'] = array(
      '#type' => 'textfield',
      '#title' => 'check_plain($type->title_label)',
      '#required' => TRUE,
      // '#default_value' => $node->title,
      '#maxlength' => 255,
      // The label might be missing if the Title module has replaced it.
      '#weight' => !empty($extra_fields['title']) ? $extra_fields['title']['weight'] : -5,
    );

    field_attach_form('newsletter_assembler_child', $newsletter_assembler_child_type, $entity_form, $form_state, LANGUAGE_NONE);

    return $entity_form;
  }
}
