<?php

/**
 * @file
 * Defines the inline entity form controller for Newsletter Assembler.
 */

class NewsletterAssemblerBaseInlineEntityFormController extends EntityInlineEntityFormController {

  /**
   * Overrides EntityInlineEntityFormController::defaultLabels().
   */
  public function defaultLabels() {
    $labels = array(
      'singular' => t('Newsletter assembler Base'),
      'plural'   => t('Newsletters assembler Bases'),
    );

    return $labels;
  }

  /**
   * Overrides EntityInlineEntityFormController::entityForm().
   */
  public function entityForm($entity_form, &$form_state) {
    global $user;

    $newsletter_a = $entity_form['#entity'];
    $newsletter_assembler_child_type = newsletter_assembler_type_load($newsletter_a->type);

    $newsletter_assembler_child = (object) ((array) $newsletter_assembler_child_type + array(
      'uid'  => $user->uid,
      'name' => $user->name,
    ));

    newsletter_assembler_object_prepare($newsletter_assembler_child);

    field_attach_form('newsletter_assembler_base', $newsletter_assembler_child, $entity_form, $form_state, LANGUAGE_NONE);

    return $entity_form;
  }
}
